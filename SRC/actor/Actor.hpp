#include <iostream>
#include <cstring>

using namespace std;

class Actor {

private:


public:

//Constructeur
Actor();
Actor(string name, string firstName, string adress);

//Destructeur
~Actor();

//Getter
string getName() const;
string getFirstName() const;
string getAdress() const;

//Setter
void setName( string const name);
void setFirstname( string const firstName);
void setAdress( string const adress);

protected:

string name;
string firstName;
string adress;


};