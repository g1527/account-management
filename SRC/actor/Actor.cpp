#include <iostream>
#include <cstring>

#include "Actor.hpp"


//Implémentation

Actor::Actor(){};

Actor::Actor(string name, string firstName, string adress)
{
    this->name = name;
    this->firstName = firstName;
    this->adress = adress;
}

//Implémentation du déstructeur
Actor::~Actor(){};

//Implémentation Getter 

string Actor::getName() const
{
    return this->name;

}

string Actor::getFirstName() const
{
    return this->firstName;

}

string Actor::getAdress() const
{
    return this->adress;

}

//Implémentation Setter

void Actor::setName( string const name)
{
     this->name = name;
}

void Actor::setFirstname( string const firstname)
{
     this->name = firstname;
}

void Actor::setAdress( string const adress)
{
     this->adress = adress;
}