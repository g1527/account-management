#include <iostream>
#include <cstring>

#include "actor/Actor.hpp";


class Customer: public Actor {

private:
public:
Customer();
Customer(string name, string firstName, string adress);

//Déstructeur
~Customer();

protected:

string name;
string firstName;
string adress;


};


