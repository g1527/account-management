#include <iostream>
#include <cstring>

#include "./actor/customer/Customer.hpp"

//Implémentation constructeur
Customer::Customer(){};
Customer::Customer(string name, string firstName, string adress):Actor(name, firstName, adress){};

//Implémentation du déstructeur
Customer::~Customer(){};