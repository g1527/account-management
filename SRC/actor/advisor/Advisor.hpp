#include <iostream>
#include <cstring>
#include <cstring>

#include "actor/Actor.hpp"

using namespace std;

class Advisor:public Actor{

private:


public:
//Constructeur
Advisor();
Advisor(string name, string firstName, string adress);

//Destructeur
~Advisor();

protected:

string name;
string firstName;
string adress;

};
