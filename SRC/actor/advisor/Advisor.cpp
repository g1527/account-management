#include <iostream>
#include <cstring>

#include "./actor/advisor/Advisor.hpp"


//Implémentation constructeur
Advisor::Advisor(){};
Advisor::Advisor(string name, string firstName, string adress):Actor(name, firstName, adress){};

//Implémentation du déstructeur
Advisor::~Advisor(){};